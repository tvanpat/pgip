# Pgip

Repo for the Pretty Good Image Processing (PGIP) Project.

This project is to quickly assess and piroritize images for exploitation. 

The project is run in a docker container.  

1.  To start end the command line and run "docker-compose build", after it is done building run "docker-compose up"
2.  Drag and drop the folder of images into the project folder.
3. Enter the notebook using the hyperlink in the terminal.
4.  Go to the "Data-Triage" notebook and select the Kernal tab and choose "Restart Kernal and Run All Cells"
5. Once complete there will be an excel spreadsheet in the project folder.

