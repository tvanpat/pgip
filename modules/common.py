import cv2

from modules.dataIngest import *
from modules.yolo_models import *
from modules.saveexcel import *


DECT_PERSONS_LABELS_FILE='/home/jovyan/setupfiles/yolo_files/coco.names'
DECT_PERSONS_CONFIG_FILE='/home/jovyan/setupfiles/yolo_files/yolov3-spp.cfg'
DECT_PERSONS_WEIGHTS_FILE='/home/jovyan/setupfiles/yolo_files/yolov3-spp.weights'
DECT_PERSONS_net = cv2.dnn.readNetFromDarknet(DECT_PERSONS_CONFIG_FILE, DECT_PERSONS_WEIGHTS_FILE)

DECT_HANDGUN_LABELS_FILE='/home/jovyan/setupfiles/yolo_files/handgun.names'
DECT_HANDGUN_CONFIG_FILE='/home/jovyan/setupfiles/yolo_files/yolov3-handguns.cfg'
DECT_HANDGUN_WEIGHTS_FILE='/home/jovyan/setupfiles/yolo_files/yolov3-handguns_final.weights'
DECT_HANDGUN_net = cv2.dnn.readNetFromDarknet(DECT_HANDGUN_CONFIG_FILE, DECT_HANDGUN_WEIGHTS_FILE)

@preprocess_log
def start_pipeline(datadf):
    return datadf.copy()

@preprocess_log
def dect_faces(datadf):
    datadf['NumberofFaces'] = np.vectorize(face_dect)(datadf['imagePath'])
    return(datadf)

@preprocess_log
def dect_text(datadf):
    datadf['text'] = np.vectorize(core_ocr)(datadf['imagePath'])
    return(datadf)

@preprocess_log
def text_in_image(datadf):
    datadf['textInImage'] = np.vectorize(imageText)(datadf['text'])
    return(datadf)

@preprocess_log
def lang_detect(datadf):
    datadf['lang'] = np.vectorize(initial_lang_detect)(datadf['text'], datadf['imagePath'])
    return(datadf)

@preprocess_log
def dect_persons(datadf):
    datadf['NumberofPersons'] = np.vectorize(person_dect2)(datadf['imagePath'], DECT_PERSONS_LABELS_FILE, DECT_PERSONS_net)
    return(datadf)

@preprocess_log
def dect_handguns(datadf):
    datadf['NumberofHandguns'] = np.vectorize(handgun_dect)(datadf['imagePath'], DECT_HANDGUN_LABELS_FILE, DECT_HANDGUN_net)
    return(datadf)

@preprocess_log
def save_data(datadf):
   create_workbook(datadf)
   return(datadf)