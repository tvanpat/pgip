import glob
import datetime as dt
from pathlib import Path
import numpy as np
import cv2
from PIL import Image
import pytesseract
from random import randint

from polyglot.detect import Detector
from polyglot.detect.base import logger as polyglot_logger
polyglot_logger.setLevel("ERROR")



def getImagePath():
    img_path = set()
    ignore_dir = set(['/home/jovyan/modules', '/home/jovyan/setupfiles', '/home/jovyan/.cache', 
                      '/home/jovyan/.config', '/home/jovyan/.cache', '/home/jovyan/.ipynb_checkpoints', 
                     '/home/jovyan/.ipython', '/home/jovyan/.jupyter', '/home/jovyan/.local'])
    image_types = set(['.jpg', '.png', '.bmp'])
    for path in Path('/home/jovyan').rglob(f'*.*'):
        if path.parent not in ignore_dir:
            if path.suffix.lower() in image_types:
                img_path.add(str(path))
    return img_path

def preprocess_log(f):
    def wrapper(dataf, *args, **kwargs):
        tic = dt.datetime.now()
        result = f(dataf, *args, **kwargs)
        toc = dt.datetime.now()
        print(f"{f.__name__} took {toc - tic}")
        return result
    return wrapper

def cv_open_image(data_path):
    with open(data_path, "rb") as f:
        img_bytes = f.read()
    return(img_bytes)

def face_dect(data_path):
    img_bytes = cv_open_image(data_path)
    nparr = np.frombuffer(img_bytes, np.uint8) #img_bytes is the image name
    img_np = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    gray = cv2.cvtColor(img_np, cv2.COLOR_BGR2GRAY)
    faceCascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")
    #Detect faces in imageS
    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags = cv2.CASCADE_SCALE_IMAGE
    )
    return(len(faces))

def pil_open_image(data_path):
    image_data = Image.open(f'{data_path}')
    return image_data

def core_ocr(data_path):
    image_data = pil_open_image(data_path)
    eng_ocr = pytesseract.image_to_string(image_data)
    if eng_ocr in [' \n\x0c', '\x0c']:
        return('No Text')
    
    elif eng_ocr == None:
        return('No Text')
    
    else:
        ocr_text = str(eng_ocr)
        return(ocr_text)
    
def imageText(text):
    if text != 'No Text':
        return('Text Present')
    return(text)

def lang_sort(ocr_list):
    high_lang_confidence = []
    for ocr in ocr_list:
        if ocr.get('reliable') != False:
            lang_tup = ocr.get('languages')
            lang_tup.sort(key = lambda x: x[1], reverse=True) 
            high_lang_confidence.append(lang_tup[0])

    high_lang_confidence.sort(key = lambda x: x[1], reverse=True)
    try:
        return(high_lang_confidence[0])
    except:
        return(ocr_list)

def second_lang_detect(data_path):
    current_lang = ['tur', 'ara', 'fas', 'pus']
    ocr_list = []
    image_data = pil_open_image(data_path)
    for ocr_lang in current_lang:
        ocr = pytesseract.image_to_string(image_data, lang=ocr_lang)
        initial_lang = Detector(ocr)
        cur_reliable = initial_lang.reliable
        lang_list = []
        multi_lang = initial_lang.languages
        for multi in multi_lang:
            if multi.name != 'un':
                lang_list.append((multi.name, multi.confidence))
        
        
        lang_dict = {'curr_detect_lang': ocr_lang, 'reliable': initial_lang.reliable, 'languages':lang_list}
        ocr_list.append(lang_dict)
    
    likely_lang = lang_sort(ocr_list)
    return(likely_lang[0])

def initial_lang_detect(text, imagePath):
    if text == 'No Text':
        return('N/A')
    try:
        initial_lang = Detector(text)
        if initial_lang.reliable == True:
            lang_list = []
            multi_lang = initial_lang.languages
            for multi in multi_lang:
                if multi.name != 'un':
                    lang_list.append(multi.name)
            return(", ".join(lang_list))
        else:
            text = second_lang_detect(imagePath)
            return(text)
    except:
        return('Not Enough Text')
    
def add_fake_int(text):
    x = len(text)
    return (randint(0, x))

