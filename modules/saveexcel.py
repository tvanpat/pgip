import openpyxl
from pandas import ExcelWriter
import xlsxwriter
import pandas as pd
import datetime as dt
import numpy as np

pd.options.mode.chained_assignment = None  # default='warn'




def update_image_path(imagePath):
    newImagePath = imagePath.replace('/home/jovyan/','')
    return(newImagePath)

def get_summary(df):
    summary =  {'Total Image Count': [df.shape[0]],
                'Images w/ Faces': [df[df.NumberofFaces > 0].shape[0]],
                'Images w/ Text': [df[df.text != 'No Text'].shape[0]],
                'Images w/ Handguns': [df[df.NumberofHandguns > 0].shape[0]],
                'Images w/ Persons': [df[df.NumberofPersons > 0].shape[0]],
                'Priority Images': [df[((df.NumberofFaces > 0) | (df.NumberofPersons > 0)) & (df.NumberofHandguns > 0)].shape[0]]}
    
    return pd.DataFrame(data = summary)

def create_workbook(image_processing_df):
    curdate = dt.datetime.now()
    day_mont_year_format = f'/home/jovyan/{curdate.day}-{curdate.strftime("%b")}-{curdate.year}-{curdate.strftime("%H-%M")}'
    xl_filename = f'{day_mont_year_format}-ImageSummary.xlsx'
    
    writer = pd.ExcelWriter(xl_filename ,engine='xlsxwriter',options={'strings_to_formulas': False})
    #Get summary Dataframe
    summarydf = get_summary(image_processing_df)
    summarydf.to_excel(writer, sheet_name='summary')
    
    #Dataframe for images where faces are detected
    faces_persons_df = image_processing_df[(image_processing_df.NumberofFaces > 0) | (image_processing_df.NumberofPersons > 0)]
    faces_persons_df.drop(['text', 'textInImage', 'lang', 'NumberofHandguns'], axis = 1, inplace = True)
    faces_persons_df['imagePath'] = np.vectorize(update_image_path)(faces_persons_df['imagePath'])
    faces_persons_df.to_excel(writer, sheet_name='Faces or Persons')

    
    #Dataframe for images where text is detected
    text_df = image_processing_df[image_processing_df.text != 'No Text']
    text_df.drop(['NumberofFaces', 'text', 'textInImage', 'NumberofHandguns', 'NumberofPersons'], axis = 1, inplace = True)
    text_df['imagePath'] = np.vectorize(update_image_path)(text_df['imagePath'])
    text_df.to_excel(writer, sheet_name='Text')
    
    #Dataframe for images where handguns are detected
    handguns_df = image_processing_df[image_processing_df.NumberofHandguns > 0]
    handguns_df.drop(['NumberofFaces', 'text', 'textInImage', 'lang', 'NumberofPersons'], axis = 1, inplace = True)
    handguns_df['imagePath'] = np.vectorize(update_image_path)(handguns_df['imagePath'])
    handguns_df.to_excel(writer, sheet_name='Handguns')
    
    #Dataframe for images where both faces and weapons are detected
    highpriority_df = image_processing_df[((image_processing_df.NumberofFaces > 0) | (image_processing_df.NumberofPersons > 0)) & (image_processing_df.NumberofHandguns > 0)]
    highpriority_df.drop(['text', 'textInImage', 'lang'], axis = 1, inplace = True)
    #highpriority_df['imagePath'] = np.vectorize(update_image_path)(highpriority_df['imagePath'])
    highpriority_df.to_excel(writer, sheet_name='High Priority')
    
    
    writer.save()
